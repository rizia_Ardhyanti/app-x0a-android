package ardhyanti.rizia.appx0a

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray

class MainActivity : AppCompatActivity(), View.OnClickListener {

    var daftarmhs = mutableListOf<HashMap<String,String>>()
    var daftarprodi = mutableListOf<String>()
    var imStr = ""
    lateinit var AdapterDataMhs : AdapterDataMhs
    lateinit var adapProd : ArrayAdapter<String>
    lateinit var MediaHelper : MediaHelper
    var uri = "http://192.168.43.239/android/app0a/show_data.php"
    var uri1 = "http://192.168.43.239/android/app0a/show_prodi.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AdapterDataMhs =  AdapterDataMhs(daftarmhs)
        listMhs.layoutManager = LinearLayoutManager(this)
        listMhs.adapter = AdapterDataMhs
        listMhs.addOnItemTouchListener(itemTouch)
        adapProd = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarprodi)
        spinProdi.adapter = adapProd
        MediaHelper = MediaHelper(this)
        imUpload.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        getProdi()
        ShowMhs()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if (requestCode == MediaHelper.getRcGalery()){
                imStr = MediaHelper.getBitmapToString(data!!.data,imUpload)
            }
        }
    }

    //nampil prodi
    fun getProdi(){
        val request  = StringRequest(Request.Method.POST,uri1,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarprodi.add(jsonObject.getString("nama_prodi"))
                }
                adapProd.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    val itemTouch = object  : RecyclerView.OnItemTouchListener{
        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
        }
        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
            val view = rv.findChildViewUnder(rv.x,e.y)
            val tag = rv.getChildAdapterPosition(view!!)
            val pos = daftarprodi.indexOf(daftarmhs.get(tag).get("nama_prodi"))

            spinProdi.setSelection(pos)
            edNim.setText(daftarmhs.get(tag).get("nim").toString())
            edNamaMhs.setText(daftarmhs.get(tag).get("nama").toString())
            Picasso.get().load(daftarmhs.get(tag).get("url")).into(imUpload)
            return false
        }

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        }

    }

    fun ShowMhs(){
        val request = StringRequest(Request.Method.POST,uri,Response.Listener { response ->
            val jsonArray = JSONArray(response)
            for (x in 0..(jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var mhs = HashMap<String,String>()
                mhs.put("nim",jsonObject.getString("nim"))
                mhs.put("nama",jsonObject.getString("nama"))
                mhs.put("nama_prodi",jsonObject.getString("nama_prodi"))
                mhs.put("url",jsonObject.getString("url"))
                mhs.put("tanggal_lahir",jsonObject.getString("tanggal_lahir"))
                mhs.put("jenis_kelamin",jsonObject.getString("jenis_kelamin"))
                mhs.put("alamat",jsonObject.getString("alamat"))
                daftarmhs.add(mhs)
            }
            AdapterDataMhs.notifyDataSetChanged()
        },Response.ErrorListener { error ->
            Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_SHORT).show()
        })
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imUpload->{
                val intent  = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,MediaHelper.getRcGalery())
            }

        }
    }
}
