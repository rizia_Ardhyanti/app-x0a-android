package ardhyanti.rizia.appx0a
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import android.util.Base64
import android.widget.ImageView
import java.io.ByteArrayOutputStream

class MediaHelper(context : Context) {
    val  context = context
    fun getRcGalery(): Int{
        return Req_Code_Galery
    }

    fun bitmapTostring(bmp : Bitmap) : String{
        val outputStream =ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG,60,outputStream)
        val byteArray = outputStream.toByteArray()
        return  Base64.encodeToString(byteArray,Base64.DEFAULT)
    }

    fun getBitmapToString(uri: Uri?, imv:ImageView) : String{
        var bmp = MediaStore.Images.Media.getBitmap(this.context.contentResolver,uri)
        var dim = 720
        if(bmp.height > bmp.height){
            bmp = Bitmap.createScaledBitmap(bmp,(bmp.width*70).div(bmp.height),dim,true)
        }else{
            bmp = Bitmap.createScaledBitmap(bmp,dim, (bmp.height*dim).div(bmp.width),true)
        }
        imv.setImageBitmap(bmp)
        return bitmapTostring(bmp)
    }


    companion object{
        const val Req_Code_Galery = 100
    }
}